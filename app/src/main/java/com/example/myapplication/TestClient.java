package com.example.myapplication;

import java.util.List;
import java.util.Map;

import org.ros.internal.message.RawMessage;
import org.ros.message.Time;
import org.ros.namespace.GraphName;
import org.ros.node.AbstractNodeMain;
import org.ros.node.ConnectedNode;
import org.ros.internal.message.Message;
import org.ros.message.Duration;
import actionlib_msgs.GoalStatusArray;
import actionlib_msgs.GoalID;
import actionlib_msgs.GoalStatus;
import std_msgs.String;
import sweetie_bot_control_msgs.FollowStepSequenceActionFeedback;
import sweetie_bot_control_msgs.FollowStepSequenceActionGoal;
import sweetie_bot_control_msgs.FollowStepSequenceActionResult;
import sweetie_bot_control_msgs.FollowStepSequenceFeedback;
import sweetie_bot_control_msgs.FollowStepSequenceGoal;
import sweetie_bot_control_msgs.FollowStepSequenceResult;
import sweetie_bot_control_msgs.RigidBodyTrajectory;
import sweetie_bot_control_msgs.RigidBodyTrajectoryPoint;

import org.apache.commons.logging.Log;
import org.ros.node.Node;
import org.ros.node.NodeMain;
import org.ros.node.parameter.ParameterTree;
import org.ros.node.topic.Publisher;
//import org.ros.node.parameter.ParameterTree;

import com.github.ekumen.rosjava_actionlib.ActionClientListener;
import com.github.ekumen.rosjava_actionlib.ActionClient;
import com.github.ekumen.rosjava_actionlib.ClientStateMachine;

/**
 * Class to test the actionlib client.
 * @author Ernesto Corbellini ecorbellini@ekumenlabs.com
 */
//public class TestClient extends AbstractNodeMain implements ActionClientListener<FollowStepSequenceActionFeedback, FollowStepSequenceActionResult> {
public class TestClient implements NodeMain {
    static {
        // comment this line if you want logs activated
        System.setProperty("org.apache.commons.logging.Log",
                "org.apache.commons.logging.impl.NoOpLog");
    }

    private FollowStepSequenceActionGoal goalMessage;
    private ActionClient ac = null;
    private ConnectedNode n = null;
    private volatile boolean resultReceived = false;
    private Log log;
    private java.lang.String param_name;
    private java.lang.String node_name;

    Publisher<FollowStepSequenceActionGoal> publisher = null;

    void TestClient(String name) {
    }


    public TestClient(java.lang.String node, java.lang.String param) {
        node_name = node;
        param_name = param;
    }

    @Override
    public GraphName getDefaultNodeName() {
        return GraphName.of(node_name);
    }

    @Override
    public void onStart(ConnectedNode node) {

        assert node != null;
        n = node;

        publisher = node.newPublisher("/motion/controller/step_sequence/goal", sweetie_bot_control_msgs.FollowStepSequenceActionGoal._TYPE);


        /*
        ac = new ActionClient<FollowStepSequenceActionGoal,
                FollowStepSequenceActionFeedback,
                FollowStepSequenceActionResult>
                (node, "/motion/controller/step_sequence",
                        FollowStepSequenceActionGoal._TYPE,
                        FollowStepSequenceActionFeedback._TYPE,
                        FollowStepSequenceActionResult._TYPE);

        GoalID gid;
        Duration serverTimeout = new Duration(20);
        boolean serverStarted;

        log = node.getLog();
        // Attach listener for the callbacks
        ac.attachListener(this);
        android.util.Log.i("MyApp", "\nWaiting for action server to start...");
        serverStarted = ac.waitForActionServerToStart(new Duration(20));
        if (serverStarted) {
            android.util.Log.i("MyApp", "Action server started.\n");
        }
        else {
            android.util.Log.i("MyApp", "No actionlib server found after waiting for " + serverTimeout.totalNsecs()/1e9 + " seconds!");
            System.exit(1);
        }
        */

        //*
        ParameterTree params = node.getParameterTree();

        if (!params.has(param_name)) {
            android.util.Log.i("MyApp", "No params!");
            System.exit(2);
        }
        Map<String, Object> subtree = (Map<String, Object>) params.getMap(param_name);

        //android.util.Log.i("MyApp", subtree.toString());


        //goalMessage = (FollowStepSequenceActionGoal) ac.newGoalMessage();
        //goalMessage.getGoal().getBaseMotion().toRawMessage().a
        goalMessage = publisher.newMessage();

        android.util.Log.i("goalMessage", goalMessage.toString());

        ParamToMsg param2msg = new <FollowStepSequenceActionGoal>ParamToMsg(node);

        param2msg.convert(subtree, goalMessage);

        FollowStepSequenceGoal sdf = goalMessage.getGoal();
        android.util.Log.i("goalMessage", goalMessage.toString());

        android.util.Log.i("goalMessage", goalMessage.toRawMessage().toString());
        android.util.Log.i("goalMessage", sdf.toRawMessage().getType());
        //*/

        /*
        // Create FollowStepSequence goal message
        goalMessage = (FollowStepSequenceActionGoal)ac.newGoalMessage();
        FollowStepSequenceGoal FollowStepSequenceGoal = goalMessage.getGoal();
        // set FollowStepSequence parameter
        FollowStepSequenceGoal.setOrder(3);
        System.out.println("Sending goal...");
        ac.sendGoal(goalMessage);
        gid = ac.getGoalId(goalMessage);
        System.out.println("Sent goal with ID: " + gid.getId());
        System.out.println("Waiting for goal to complete...");
        while (ac.getGoalState() != ClientStateMachine.ClientStates.DONE) {
            sleep(1);
        }
        System.out.println("Goal completed!\n");

        System.out.println("Sending a new goal...");
        ac.sendGoal(goalMessage);
        gid = ac.getGoalId(goalMessage);
        System.out.println("Sent goal with ID: " + gid.getId());
        System.out.println("Cancelling this goal...");
        ac.sendCancel(gid);
        while (ac.getGoalState() != ClientStateMachine.ClientStates.DONE) {
            sleep(1);
        }
        System.out.println("Goal cancelled succesfully.\n");
        System.out.println("Bye!");
        //System.exit(0);
        // */
    }

    @SuppressWarnings("unchecked")
    public void doMagic() {
        // Create FollowStepSequence goal message
        //FollowStepSequenceActionGoal goalMessage;
        GoalID gid;


        //goalMessage = (FollowStepSequenceActionGoal)ac.newGoalMessage();
        //FollowStepSequenceGoal FollowStepSequenceGoal = goalMessage.getGoal();
        //goalMessage
        // set FollowStepSequence parameter
        //FollowStepSequenceGoal.setOrder(3);
        android.util.Log.i("MyApp", "Sending goal...");
        Time now = n.getCurrentTime();
        goalMessage.getHeader().setStamp(now);
        goalMessage.getGoal().getHeader().setStamp(now);
        goalMessage.getGoalId().setStamp(now);
        goalMessage.getGoalId().setId("/random-" + Integer.toString(now.secs));
        publisher.publish(goalMessage);
        /*
        ac.sendGoal(goalMessage);
        gid = ac.getGoalId(goalMessage);
        android.util.Log.i("MyApp", "Sent goal with ID: " + gid.getId());
        android.util.Log.i("MyApp", "Waiting for goal to complete...");
        while (ac.getGoalState() != ClientStateMachine.ClientStates.DONE) {
            sleep(1);
        }
        android.util.Log.i("MyApp", "Goal completed!\n");
        */
    }

    @Override
    public void onShutdown(Node node) {
    }


    @Override
    public void onShutdownComplete(Node node) {
    }

    @Override
    public void onError(Node node, Throwable throwable) {
    }

    /*
    @Override
    public void resultReceived(FollowStepSequenceActionResult message)
    {
        FollowStepSequenceResult result = message.getResult();
        android.util.Log.i("MyApp", result.toString());
        //int[] sequence = result.getSequence();
        //int i;

        //resultReceived = true;
        //android.util.Log.i("MyApp", "Got FollowStepSequence result sequence: ");
        //for (i=0; i<sequence.length; i++)
        //    android.util.Log.i("MyApp", Integer.toString(sequence[i]) + " ");
        //android.util.Log.i("MyApp", "");
    }

    @Override
    public void feedbackReceived(FollowStepSequenceActionFeedback message) {
        FollowStepSequenceFeedback result = message.getFeedback();

        android.util.Log.i("MyApp", result.getTimeFromStart().toString());

        //int[] sequence = result.getSequence();
        //int i;

        //android.util.Log.i("MyApp", "Feedback from FollowStepSequence server: ");
        //for (i=0; i<sequence.length; i++)
        //    android.util.Log.i("MyApp", Integer.toString(sequence[i]) + " ");
        //android.util.Log.i("MyApp", "\n");
    }
    //*/


    /*
    @Override
    public void statusReceived(GoalStatusArray status) {
        List<GoalStatus> statusList = status.getStatusList();
        for(GoalStatus gs:statusList) {
            android.util.Log.i("MyApp", "GoalID: " + gs.getGoalId().getId() + " -- GoalStatus: " + gs.getStatus() + " -- " + gs.getText());
        }
        android.util.Log.i("MyApp", "Current state of our goal: " + ClientStateMachine.ClientStates.translateState(ac.getGoalState()));
    }


    void sleep(long msec) {
        try {
            Thread.sleep(msec);
        }
        catch (InterruptedException ex) {
        }
    } */

}