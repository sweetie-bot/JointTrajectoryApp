package com.example.myapplication;

import org.ros.android.AppCompatRosActivity;
import org.ros.node.NodeConfiguration;
import org.ros.node.NodeMainExecutor;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.Random;


public class MainActivity extends AppCompatRosActivity {

    private TestClient right_turn;
    private TestClient left_turn;
    private TestClient forward;
    private TestClient right_turn90;
    private TestClient left_turn90;


    public MainActivity() {
        // The RosActivity constructor configures the notification title and ticker
        // messages.
        super("Sweetie bot control app", "Sweetie bot control app");
    }

    @Override
    protected void init(NodeMainExecutor nodeMainExecutor) {
        Random rand = new Random();

        right_turn = new TestClient("FollowStepSequence_client_right_"+Integer.toString(rand.nextInt(50)), "/move/turn/right45");
        left_turn = new TestClient("FollowStepSequence_client_left_"+Integer.toString(rand.nextInt(50)), "/move/turn/left45");
        right_turn90 = new TestClient("FollowStepSequence_client_right_"+Integer.toString(rand.nextInt(50)), "/move/turn/right90");
        left_turn90 = new TestClient("FollowStepSequence_client_left_"+Integer.toString(rand.nextInt(50)), "/move/turn/left90");
        forward = new TestClient("FollowStepSequence_client_forward"+Integer.toString(rand.nextInt(50)), "/move/go/forward");
        // At this point, the user has already been prompted to either enter the URI
        // of a master to use or to start a master locally.

        // The user can easily use the selected ROS Hostname in the master chooser
        String host = getRosHostname();
        android.util.Log.i("MyApp Host", host);
        NodeConfiguration nodeConfiguration = NodeConfiguration.newPublic(host);
        nodeConfiguration.setMasterUri(getMasterUri());
        nodeMainExecutor.execute(right_turn, nodeConfiguration);
        nodeMainExecutor.execute(left_turn, nodeConfiguration);
        nodeMainExecutor.execute(right_turn90, nodeConfiguration);
        nodeMainExecutor.execute(left_turn90, nodeConfiguration);
        nodeMainExecutor.execute(forward, nodeConfiguration);


        final Button btn_r = findViewById(R.id.btnRight);
        btn_r.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                //btn.setActivated(false);
                //btn.setEnabled(false);
                btn_r.setClickable(false);
                right_turn.doMagic();
                //btn.setEnabled(true);
                //btn.setActivated(true);
                //getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                btn_r.setClickable(true);
            }
        });

        final Button btn_l = findViewById(R.id.btnLeft);
        btn_l.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                //btn.setActivated(false);
                //btn.setEnabled(false);
                btn_l.setClickable(false);
                left_turn.doMagic();
                //btn.setEnabled(true);
                //btn.setActivated(true);
                //getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                btn_l.setClickable(true);
            }
        });

        final Button btn_f = findViewById(R.id.btnForward);
        btn_f.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                //btn.setActivated(false);
                //btn.setEnabled(false);
                btn_f.setClickable(false);
                forward.doMagic();
                //btn.setEnabled(true);
                //btn.setActivated(true);
                //getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                btn_f.setClickable(true);
            }
        });


        final Button btn_r90 = findViewById(R.id.btnRight90);
        btn_r90.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                //btn.setActivated(false);
                //btn.setEnabled(false);
                btn_r90.setClickable(false);
                right_turn90.doMagic();
                //btn.setEnabled(true);
                //btn.setActivated(true);
                //getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                btn_r90.setClickable(true);
            }
        });

        final Button btn_l90 = findViewById(R.id.btnLeft90);
        btn_l90.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                //btn.setActivated(false);
                //btn.setEnabled(false);
                btn_l90.setClickable(false);
                left_turn90.doMagic();
                //btn.setEnabled(true);
                //btn.setActivated(true);
                //getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                btn_l90.setClickable(true);
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
