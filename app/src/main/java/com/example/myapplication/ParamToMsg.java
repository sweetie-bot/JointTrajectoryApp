package com.example.myapplication;

import android.annotation.SuppressLint;

import org.apache.commons.lang.ArrayUtils;
import org.ros.internal.message.Message;
import org.ros.internal.message.RawMessage;
import org.ros.internal.message.field.Field;
import org.ros.internal.message.field.PrimitiveFieldType;
import org.ros.node.ConnectedNode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

class ParamToMsg<T>
{
    private ConnectedNode node;

    ParamToMsg(ConnectedNode n){
        node = n;
    }

    @SuppressLint("LongLogTag")
    void convert(Map<String, Object> param_subtree, T msg) {

        RawMessage raw = ((Message) msg).toRawMessage();
        android.util.Log.i("ParamToMsg getType()", raw.getType());
        /*
        android.util.Log.i("ParamToMsg ","====== msg "+ msg.getClass().getName() +"=======");
        //android.util.Log.i("ParamToMsg getDefinition()", raw.getDefinition());
        android.util.Log.i("ParamToMsg getIdentifier()", raw.getIdentifier().getName());
        android.util.Log.i("ParamToMsg getIdentifier()", raw.getIdentifier().toString());
        android.util.Log.i("ParamToMsg getFields()", raw.getFields().toString());
        //*/

        List<Field> fields = raw.getFields();
        for(Field field : fields)
        {
            /*
            android.util.Log.i("ParamToMsg ","====== field "+ field.getName() +"=======");
            android.util.Log.i("ParamToMsg field toString()", field.toString());
            android.util.Log.i("ParamToMsg field getType().getName()", field.getType().getName());
            android.util.Log.i("ParamToMsg field getType().toString()", field.getType().toString());
            android.util.Log.i("ParamToMsg field getJavaTypeName()", field.getType().getJavaTypeName());
            android.util.Log.i("ParamToMsg field hashCode()", Integer.toString(field.getValue().hashCode()));

            android.util.Log.i("ParamToMsg field getValue()", field.getValue().toString());
            android.util.Log.i("ParamToMsg field getClass()", field.getValue().getClass().toString());
            android.util.Log.i("ParamToMsg field getCanonicalName()", field.getValue().getClass().getCanonicalName());
            //*/

            String canonical_name = field.getValue().getClass().getCanonicalName();

            if(!param_subtree.containsKey(field.getName()))
            {
                android.util.Log.i("ParamToMsg field key not found", field.getName());
                continue;
            }

            assert canonical_name != null;
            if(canonical_name.endsWith("ArrayList"))
            {
                Object obj[] = null;
                try {
                    obj = (Object[]) param_subtree.get(field.getName());
                } catch (Exception e) {
                    android.util.Log.i("ParamToMsg list error", e.getMessage());
                }
                assert obj != null;


                ArrayList<Object> list = field.getValue();

                for(int i = 0; i<obj.length; i++){
                    Object rbtp = node.getTopicMessageFactory().newFromType(field.getType().getName());
                    list.add(rbtp);
                    convert((Map<String, Object>) obj[i], (T) list.get(i));
                }
                continue;
            }


            if(canonical_name.endsWith("]")) {
                Object obj[] = null;
                try {
                    obj = (Object[]) param_subtree.get(field.getName());
                    assert obj != null;
                } catch (Exception e) {
                    android.util.Log.i("ParamToMsg array error", e.getMessage());
                }

                switch (canonical_name) {
                    case "boolean[]":
                        field.setValue( ArrayUtils.toPrimitive(Arrays.copyOf(obj, obj.length, Boolean[].class)) );
                        break;
                    case "byte[]":
                        field.setValue( ArrayUtils.toPrimitive( Arrays.copyOf( obj, obj.length, Byte[].class)));
                        break;
                    case "double[]":
                        field.setValue( ArrayUtils.toPrimitive( Arrays.copyOf( obj, obj.length, Double[].class)));
                        break;
                    case "float[]":
                        field.setValue( ArrayUtils.toPrimitive( Arrays.copyOf( obj, obj.length, Float[].class)));
                        break;
                    case "integer[]":
                        field.setValue( ArrayUtils.toPrimitive( Arrays.copyOf( obj, obj.length, Integer[].class)));
                        break;
                    case "long[]":
                        field.setValue( ArrayUtils.toPrimitive( Arrays.copyOf( obj, obj.length, Long[].class)));
                        break;
                    case "short[]":
                        field.setValue( ArrayUtils.toPrimitive( Arrays.copyOf( obj, obj.length, Short[].class)));
                        break;
                    default:
                        android.util.Log.i("ParamToMsg unknown array type", canonical_name);
                        break;
                }
                continue;
            }

            if(PrimitiveFieldType.existsFor(field.getType().getName()))
            {
                Object time = param_subtree.get(field.getName());
                field.setValue(time);
                continue;
            }
            convert((Map<String, Object>) param_subtree.get(field.getName()), (T) field.getValue());

        }
    }
}
